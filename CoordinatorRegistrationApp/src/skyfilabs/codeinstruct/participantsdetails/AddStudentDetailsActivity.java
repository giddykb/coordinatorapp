package skyfilabs.codeinstruct.participantsdetails;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import skyfilabs.codeinstruct.coordinatorregistrationapp.DbHandler;
import skyfilabs.codeinstruct.coordinatorregistrationapp.MainActivity;
import skyfilabs.codeinstruct.coordinatorregistrationapp.R;
import skyfilabs.codeinstruct.coordinatorregistrationapp.ViewParticipants;


import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AddStudentDetailsActivity extends Activity {
	EditText name,email,phone,college,paymentid;
	Spinner year,department;
	Button saveBtn;
	String eventid;
	int status;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_student_details);
		name = (EditText)findViewById(R.id.nameet);
		email = (EditText)findViewById(R.id.emailet);
		phone = (EditText)findViewById(R.id.phoneet);
		college = (EditText)findViewById(R.id.collegeet);
		paymentid = (EditText)findViewById(R.id.paymentid);
		year = (Spinner)findViewById(R.id.academicyear);
		department = (Spinner)findViewById(R.id.department);
		saveBtn = (Button)findViewById(R.id.savebutton);
		Intent intent = getIntent();
		eventid = intent.getExtras().getString("eventid");
		status = intent.getExtras().getInt("status");
		if(status==0){
			paymentid.setText(String.valueOf(0));
			paymentid.setVisibility(View.GONE);		
			
		}

	}
	
	public void save(View v){
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email.getText().toString().trim().toLowerCase());
		boolean flag = matcher.matches();
		String regex = "[0-9]";
		String data = phone.getText().toString().trim();
		if(name.getText().toString().equals("")){
			Toast.makeText(getApplicationContext(),"Enter Name",Toast.LENGTH_SHORT).show();
		}
		else if(email.getText().toString().trim().equals("")){
			Toast.makeText(getApplicationContext(),"Enter Email",Toast.LENGTH_SHORT).show();
		}
		else if(!flag){
			Toast.makeText(getApplicationContext(),"Enter valid email id",Toast.LENGTH_SHORT).show();
		}
		else if(phone.getText().toString().trim().equals("")){
			Toast.makeText(getApplicationContext(),"Enter Phone",Toast.LENGTH_SHORT).show();
		}
		else if(!data.matches(regex) && data.length()<10){
			Toast.makeText(getApplicationContext(),"Enter valid Phone number",Toast.LENGTH_SHORT).show();
		}
		else if(college.getText().toString().equals("")){
			Toast.makeText(getApplicationContext(),"Enter college name",Toast.LENGTH_SHORT).show();
		}
		else if(paymentid.getText().toString().trim().equals("")){
			Toast.makeText(getApplicationContext(),"Enter Payment",Toast.LENGTH_SHORT).show();
		}
		else if(year.getSelectedItem().toString().equals("Select Year")){
			Toast.makeText(getApplicationContext(),"Select Academic Year",Toast.LENGTH_SHORT).show();
		}
		else if(department.getSelectedItem().toString().equals("Select Department")){
			Toast.makeText(getApplicationContext(),"Select Department",Toast.LENGTH_SHORT).show();
		}
		
		else{
		
		DbHandler db = new DbHandler(getApplicationContext());
		Student student = new Student();
		student.setName(name.getText().toString());
		student.setEmail(email.getText().toString());
		student.setPhone(phone.getText().toString());
		student.setCollege(college.getText().toString());
		student.setPaymentid(paymentid.getText().toString());
		student.setYear(year.getSelectedItem().toString());
		student.setEventid(eventid);
		student.setDepartmrnt(department.getSelectedItem().toString());
		long l = db.addStudent(student);
		if(l!=-1){
			Toast.makeText(getApplicationContext(),"Details Saved",Toast.LENGTH_SHORT).show();
			name.setText("");
			email.setText("");
			phone.setText("");
			college.setText("");
			paymentid.setText("0");
			year.setSelection(0);
			
		}else{
			Toast.makeText(getApplicationContext(),"Errors Occurred",Toast.LENGTH_SHORT).show();
		}
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_student_details, menu);
		return true;
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==R.id.menu_home){
			Intent intent = new Intent(getApplicationContext(),MainActivity.class);
			startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}
	
	

}
