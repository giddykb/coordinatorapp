package skyfilabs.codeinstruct.coordinatorregistrationapp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import skyfilabs.codeinstruct.participantsdetails.Student;



import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class EditStudentDetails extends Activity {
	EditText name,email,phone,college,paymentet;
	Spinner year,departmentspn;
	Button updateBtn;
	String name1,email1,phone1,teamid,college1,year1,trainerid,studentid,branch,eventid,workshopname,venue,department,payment;
	int id;
	TextView eventDetail_text;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_student_details);
		paymentet = (EditText)findViewById(R.id.payment);
		name = (EditText)findViewById(R.id.nameet1);
		email = (EditText)findViewById(R.id.emailet1);
		phone = (EditText)findViewById(R.id.phoneet1);
		college = (EditText)findViewById(R.id.collegeet1);
		
		year = (Spinner)findViewById(R.id.academicyear1);
		departmentspn = (Spinner)findViewById(R.id.department2);
		updateBtn = (Button)findViewById(R.id.updatebutton);
		eventDetail_text = (TextView)findViewById(R.id.eventDetail_text);
		Intent intent = getIntent();
		DbHandler db = new DbHandler(getApplicationContext());
		boolean flag = db.getPaymentStatus(Integer.parseInt(intent.getExtras().getString("eventid")));
		if(!flag){
			paymentet.setText(String.valueOf(0));
			paymentet.setVisibility(View.GONE);
		}
		name1 = intent.getExtras().getString("name");
		email1 = intent.getExtras().getString("email");
		phone1 = intent.getExtras().getString("phone");
		payment = intent.getExtras().getString("payment");
		college1 = intent.getExtras().getString("college");
		year1 = intent.getExtras().getString("year");
		studentid = intent.getExtras().getString("studentid");
		branch = intent.getExtras().getString("branch");
		eventid = intent.getExtras().getString("eventid");
		workshopname = intent.getExtras().getString("workshopname");
		venue = intent.getExtras().getString("venue");
		id = intent.getExtras().getInt("id");
		department = intent.getExtras().getString("department");
		eventDetail_text.setText(workshopname+" by "+branch+" at "+venue);
		name.setText(name1);
		email.setText(email1);
		phone.setText(phone1);
		college.setText(college1);
		paymentet.setText(payment);
		
		
		if(year1.equals("year")){
			year.setSelection(0);
		}
		if(year1.equals("I Year")){
			year.setSelection(1);
		}
		if(year1.equals("II Year")){
			year.setSelection(2);
		}
		if(year1.equals("III Year")){
			year.setSelection(3);
		}
		if(year1.equals("IV Year")){
			year.setSelection(4);
		}
		
		if(department.equals("department")){
			departmentspn.setSelection(0);
		}
		
		if(department.equals("Electronics(ECE)")){
			departmentspn.setSelection(1);
		}
		if(department.equals("Computer Science(CSE)")){
			departmentspn.setSelection(2);
		}
		if(department.equals("Information Technology(IT)")){
			departmentspn.setSelection(3);
		}
		if(department.equals("Civil Engineering")){
			departmentspn.setSelection(4);
		}
		if(department.equals("Mechanical Engineering")){
			departmentspn.setSelection(5);
		}
		if(department.equals("Aeronautical Engineering")){
			departmentspn.setSelection(6);
		}
		if(department.equals("Industrial Engineering")){
			departmentspn.setSelection(7);
		}
		if(department.equals("Mechatronics Engineering")){
			departmentspn.setSelection(8);
		}
		if(department.equals("Automobile Engineering")){
			departmentspn.setSelection(9);
		}
		if(department.equals("Electrical Engineering")){
			departmentspn.setSelection(10);
		}
		if(department.equals("Petroleum Engineering")){
			departmentspn.setSelection(11);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_student_details, menu);
		return true;
	}
	public void update(View v){
		//student.setDepartment(department);
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email.getText().toString().trim());
		boolean flag = matcher.matches();
		String regex = "[0-9]";
		String data = phone.getText().toString();
		if(name.getText().toString().equals("")){
			Toast.makeText(getApplicationContext(),"Enter Name",Toast.LENGTH_SHORT).show();
		}
		else if(email.getText().toString().trim().equals("")){
			Toast.makeText(getApplicationContext(),"Enter Email",Toast.LENGTH_SHORT).show();
		}
		else if(!flag){
			Toast.makeText(getApplicationContext(),"Enter valid email id",Toast.LENGTH_SHORT).show();
		}
		else if(phone.getText().toString().equals("")){
			Toast.makeText(getApplicationContext(),"Enter Phone",Toast.LENGTH_SHORT).show();
		}
		else if(!data.matches(regex) && data.length()<10){
			Toast.makeText(getApplicationContext(),"Enter valid Phone number",Toast.LENGTH_SHORT).show();
		}
		else if(college.getText().toString().equals("")){
			Toast.makeText(getApplicationContext(),"Enter college name",Toast.LENGTH_SHORT).show();
		}
		
		else if(year.getSelectedItem().toString().equals("Select Year")){
			Toast.makeText(getApplicationContext(),"Select Academic Year",Toast.LENGTH_SHORT).show();
		}
		else if(departmentspn.getSelectedItem().toString().equals("Select Department")){
			Toast.makeText(getApplicationContext(),"Select Department",Toast.LENGTH_SHORT).show();
		}
		else{
			Student student = new Student();
			student.setId(id);
			student.setName(name.getText().toString());
			student.setEmail(email.getText().toString());
			student.setPhone(phone.getText().toString());
			student.setCollege(college.getText().toString());
			student.setDepartmrnt(departmentspn.getSelectedItem().toString());
			student.setYear(year.getSelectedItem().toString());
			student.setVenue(venue);
			student.setPaymentid(paymentet.getText().toString());
			student.setEventid(eventid);
			student.setWorkshopname(workshopname);
			student.setBranch(branch);
			DbHandler db = new DbHandler(getApplicationContext());
			int i = db.updateStudentDetails(student);
			if(i!=0){
				Toast.makeText(getApplicationContext(),"Record Updated",Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(getApplicationContext(),ViewParticipants.class);
				intent.putExtra("eventid",eventid);
				intent.putExtra("branch",branch);
				intent.putExtra("workshopname",workshopname);
				intent.putExtra("venue",venue);
				startActivity(intent);
				
			}else{
				Toast.makeText(getApplicationContext(),"Errors Occurred",Toast.LENGTH_SHORT).show();
			}
			
		}
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==R.id.menu2_home){
			Intent intent = new Intent(getApplicationContext(),MainActivity.class);
			startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}
}
