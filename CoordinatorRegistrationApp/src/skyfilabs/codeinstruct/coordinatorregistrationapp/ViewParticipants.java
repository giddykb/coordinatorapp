package skyfilabs.codeinstruct.coordinatorregistrationapp;

import java.util.ArrayList;

import skyfilabs.codeinstruct.participantsdetails.Student;
import skyfilabs.codeinstruct.participantsdetails.StudentDetailsAdapter;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


public class ViewParticipants extends Activity {
	ListView list;
	ArrayList<Student> students_List;
	String eid;
	String venue;
	String workshopname;
	String branch;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_participants);
		list = (ListView)findViewById(R.id.participants_list);
		Intent intent = getIntent();
		eid = intent.getExtras().getString("eventid");
		venue = intent.getExtras().getString("venue");
		workshopname = intent.getExtras().getString("workshopname");
		branch = intent.getExtras().getString("branch");
		
		
		students_List = getStudentsList();
		StudentDetailsAdapter adapter = new StudentDetailsAdapter(getApplicationContext(),R.layout.participantdetail, students_List);
		list.setAdapter(adapter);
		OnItemClickListener listener = new OnItemClickListener() {
                                                                  
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Student student  = students_List.get(position);
				Intent intent = new Intent(getApplicationContext(),EditStudentDetails.class);
				intent.putExtra("department",student.getDepartmrnt());
				intent.putExtra("name",student.getName());
				intent.putExtra("email",student.getEmail());
				intent.putExtra("phone",student.getPhone());
				intent.putExtra("college",student.getCollege());
				intent.putExtra("id",student.getId());
				intent.putExtra("studentId", student.getStudentId());
				intent.putExtra("payment",student.getPaymentid());
				intent.putExtra("year",student.getYear());
				intent.putExtra("branch",branch);
				intent.putExtra("eventid",eid);
				intent.putExtra("workshopname",workshopname);
				intent.putExtra("venue",venue);
				startActivity(intent);				
			}
		};
		list.setOnItemClickListener(listener );
		
	}
	private ArrayList<Student> getStudentsList() {
		DbHandler db = new DbHandler(getApplicationContext());
		students_List = db.getStudents(eid);
		return students_List;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_participants, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId()==R.id.menu1_home){
			Intent intent = new Intent(getApplicationContext(),MainActivity.class);
			startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}

}
