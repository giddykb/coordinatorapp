package skyfilabs.codeinstruct.coordinatorregistrationapp;

import java.util.ArrayList;
import java.util.Locale;

import skyfilabs.codeinstruct.participantsdetails.Student;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;
public class DbHandler extends SQLiteOpenHelper{
		private static int DATABASE_VERSION = 1;
		private static final String DATABASE_NAME = "workshops1";
		private static final String TABLE_NAME1 = "coordinatordata1";
		private static final String KEY_ID = "id";
		private static final String KEY_VENUE = "venue";
		private static final String KEY_BRANCH = "branch";
		private static final String KEY_WORKSHOP = "workshop";
		private static final String KEY_EVENTID = "eventid";
		private static final String KEY_LOCATION = "location";
		private static final String KEY_COORDINATORID = "cid";
		private static final String KEY_COORDINATORSTATUS = "status";
		private static final String KEY_PASSWORD = "password";
		
		ArrayList<WorkshopData> locations = new ArrayList<WorkshopData>();
		private static final String DATABASE_CREATE1 = "create table "+TABLE_NAME1+
													  "("+KEY_ID+" INTEGER PRIMARY KEY,"
													  	+KEY_LOCATION+" TEXT,"
													     +KEY_BRANCH+" TEXT,"
													     +KEY_EVENTID+" INTEGER,"
													     +KEY_VENUE+" TEXT,"
													     +KEY_WORKSHOP+" TEXT,"
													     +KEY_COORDINATORID+" TEXT,"
													    +KEY_COORDINATORSTATUS+" INTEGER,"
													    +KEY_PASSWORD+ " TEXT)";
		
		
		private static final String TABLE_NAME2 = "studentsdata1";
		
		private static final String KEY_NAME = "name";
		private static final String KEY_PHONE = "phone_number";
		private static final String KEY_EMAIL = "email";
		private static final String KEY_COLLEGE = "college";
		private static final String KEY_YEAR = "year";
		private static final String KEY_PAYMENT = "payment";
		private static final String KEY_STUDENTID = "studentid";
		private static final String KEY_DEPARTMENT = "departmrnt";
		private static DbHandler instance;
		private static final String DATABASE_CREATE2 = "create table if not exists "+TABLE_NAME2+
													  "("+KEY_ID+" INTEGER PRIMARY KEY,"
													  	+KEY_NAME+" TEXT,"
													     +KEY_PHONE+" TEXT,"
													     +KEY_EMAIL+" TEXT,"
													     +KEY_COLLEGE+" TEXT,"
													     +KEY_YEAR+" TEXT,"
													     +KEY_PAYMENT+" TEXT,"
													     +KEY_EVENTID+" INTEGER,"
													     +KEY_STUDENTID+" TEXT,"
													     +KEY_DEPARTMENT+" TEXT)";
		
		
		
		public static int number = 0;
		ArrayList<Student> students_List = new ArrayList<Student>();
		
		Context context;
		public static synchronized DbHandler getHelper(Context context){
			if(instance ==  null){
				instance = new DbHandler(context);
			}
			return instance;
		}
		
		@Override
		public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
		if(!db.isReadOnly()){
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
		}
		
		
		public DbHandler(Context context) {
			super(context, DATABASE_NAME,null,DATABASE_VERSION);
			this.context = context;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE1);
			db.execSQL(DATABASE_CREATE2);
		}
		public long addWorkshopData(WorkshopData data){
			SQLiteDatabase db = getWritableDatabase();
				WorkshopData wd = data;
				ContentValues values = new ContentValues();
				values.put(KEY_LOCATION,wd.getLocation());
				values.put(KEY_BRANCH,wd.getBranch());
				values.put(KEY_EVENTID,Integer.parseInt(wd.getEventid()));
				values.put(KEY_VENUE,wd.getVenue());
				values.put(KEY_WORKSHOP,wd.getWorkshop());
				values.put(KEY_COORDINATORID,wd.getCoordinatoremail());
				values.put(KEY_COORDINATORSTATUS,wd.getStatus());
				values.put(KEY_PASSWORD,wd.getPassword());
				long i = db.insert(TABLE_NAME1, null, values);
			
			db.close();
			return i;
		}
		
		public void delete(){
			SQLiteDatabase db = getWritableDatabase();
			db.execSQL("delete from "+TABLE_NAME1+" where id>=1");
		}
		
		public boolean checkPassword(int eid,String password){
		   	String query = "select*from "+TABLE_NAME1+" where "+KEY_PASSWORD+" = '"+password+"' and "+KEY_EVENTID+" = "+eid;
		   	SQLiteDatabase db = this.getReadableDatabase();
		   	Cursor cursor = db.rawQuery(query, null);
		   	boolean flag = false;
		   	if(cursor.moveToFirst()){
		do{
		flag = true;
		}while(cursor.moveToNext());
		}
		cursor.close();
		db.close();
		   	
		   	
		   	return flag;
		   }
		

		
	    public ArrayList<WorkshopData> getLocationDetails(){
			locations.clear();
			String select_Query = "select distinct "+KEY_LOCATION+" from "+TABLE_NAME1+" order by "+KEY_LOCATION+" asc";
			SQLiteDatabase db = getWritableDatabase();
			Cursor cursor = db.rawQuery(select_Query, null);
			if(cursor.moveToFirst()){
				do{
					WorkshopData wd = new WorkshopData();
					wd.setLocation(cursor.getString(0));
					locations.add(wd);
				}while(cursor.moveToNext());
			}
			cursor.close();
			db.close();
			return locations;
		}
	    
	    public ArrayList<WorkshopData> getWorkshopsDetails(String email){

			locations.clear();
		  String selectQuery = "select * from "+TABLE_NAME1+" where "+KEY_COORDINATORID+" = '"+email+"'";
			
			SQLiteDatabase db = getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			
			if(cursor.moveToFirst()){
				do{
					WorkshopData wd = new WorkshopData();
					wd.setId(cursor.getInt(0));
					wd.setLocation(cursor.getString(1));
					wd.setBranch(cursor.getString(2));
					wd.setEventid(String.valueOf(cursor.getInt(3)));
					wd.setVenue(cursor.getString(4));
					wd.setWorkshop(cursor.getString(5));
					wd.setCoordinatoremail(cursor.getString(6));
					wd.setStatus(cursor.getInt(7));
					locations.add(wd);
				}while(cursor.moveToNext());
			}
			cursor.close();
			db.close();
			return locations;
		}
	    
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("drop table "+TABLE_NAME1);
			db.execSQL("drop table "+TABLE_NAME2);
			onCreate(db);	
			
		}
		public long addStudent(Student contact){
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();

			int id1 = generateId(contact.getEventid());
			int wid = Integer.parseInt(contact.getEventid());
			String sequence = String.format("%04d",id1+1);
			String id = String.format("%04d",wid);
			String uniqueid = id+sequence;
			values.put(KEY_NAME,contact.getName());
			values.put(KEY_EMAIL,contact.getEmail());
			values.put(KEY_PHONE,contact.getPhone());
			values.put(KEY_COLLEGE,contact.getCollege());
			values.put(KEY_YEAR,contact.getYear());
			values.put(KEY_PAYMENT,contact.getPaymentid());
			values.put(KEY_STUDENTID,uniqueid);
			values.put(KEY_DEPARTMENT,contact.getDepartmrnt());
			values.put(KEY_EVENTID,Integer.parseInt(contact.getEventid()));
			long i = db.insert(TABLE_NAME2, null, values);
			db.close();
			//Toast.makeText(context,"Student Added to SQLite",Toast.LENGTH_SHORT).show();
			return i;
		}
		
		public int generateId(String workshopid){
	    	int id = 0;
	    	String query = "select "+KEY_EVENTID+",count(*) from "+TABLE_NAME2+" where "+KEY_EVENTID+"="+workshopid+" group by "+KEY_EVENTID+" having count(*)>=0";
	    	SQLiteDatabase db = getWritableDatabase();
			Cursor cursor = db.rawQuery(query, null);
			if(cursor.getCount()==0){
				id = 0;
			}else{
				if(cursor.moveToFirst()){
					do{
						id = cursor.getInt(1);
					}while(cursor.moveToNext());
				}
			}
	    	return id;
	    }
		public int updateStudentDetails(Student contact){
			SQLiteDatabase db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(KEY_DEPARTMENT,contact.getDepartmrnt());
			values.put(KEY_NAME,contact.getName());
			values.put(KEY_EMAIL,contact.getEmail());
			values.put(KEY_PHONE,contact.getPhone());
			values.put(KEY_COLLEGE,contact.getCollege());
			values.put(KEY_PAYMENT,contact.getPaymentid());
			values.put(KEY_YEAR,contact.getYear());
		    int i = db.update(TABLE_NAME2,values, KEY_ID + "=" + contact.getId(), null);
		    return i;
		}
		
	    public ArrayList<Student> getStudents(String wid){
			
			students_List.clear();
			//String select_Query = "select * from "+TABLE_NAME2+" where "+KEY_EVENTID+"="+wid;
			int eid = Integer.parseInt(wid);
			String select_Query = "select * from "+TABLE_NAME2+" where "+KEY_EVENTID+" = "+eid;
			SQLiteDatabase db = getWritableDatabase();
			Cursor cursor = db.rawQuery(select_Query, null);
			if(cursor.moveToFirst()){
				do{
					Student student = new Student();
					student.setId(cursor.getInt(0));
					student.setName(cursor.getString(1));
					student.setEmail(cursor.getString(3));
					student.setPhone(cursor.getString(2));
					student.setCollege(cursor.getString(4));
					student.setYear(cursor.getString(5));
					student.setPaymentid(cursor.getString(6));
					student.setEventid(String.valueOf(cursor.getInt(7)));
					student.setStudentId(cursor.getString(8));
					student.setDepartmrnt(cursor.getString(9));
					students_List.add(student);
				}while(cursor.moveToNext());
			}
			cursor.close();
			db.close();
			return students_List;
		}
	    
	    
	    

	    public int getStudentsCount(String wid){
			
			students_List.clear();
			//String select_Query = "select * from "+TABLE_NAME2+" where "+KEY_EVENTID+"="+wid;
			int eid = Integer.parseInt(wid);
			String select_Query = "select * from "+TABLE_NAME2+" where "+KEY_EVENTID+" = "+eid;
			SQLiteDatabase db = getWritableDatabase();
			Cursor cursor = db.rawQuery(select_Query, null);
			
			return cursor.getCount();
		}
	    public ArrayList<WorkshopData> getProfilesCount() {
	    	   String countQuery = "SELECT  * FROM " + TABLE_NAME1;
	    	   SQLiteDatabase db = this.getReadableDatabase();
	    	   ArrayList<WorkshopData> list = new ArrayList<WorkshopData>();
	    	   Cursor cursor = db.rawQuery(countQuery, null);
	    	   if(cursor.moveToFirst()){
					do{
						WorkshopData wd = new WorkshopData();
						wd.setId(cursor.getInt(0));
						wd.setLocation(cursor.getString(1));
						wd.setBranch(cursor.getString(2));
						wd.setEventid(String.valueOf(cursor.getInt(3)));
						wd.setVenue(cursor.getString(4));
						wd.setWorkshop(cursor.getString(5));
						wd.setCoordinatoremail(cursor.getString(6));
						list.add(wd);
					}while(cursor.moveToNext());
				}
				cursor.close();
				db.close();
	    	   return list;
	    }
	    public String[] getPaymentDetail(int eid){
	    	String query = "select count(*),sum(payment) from "+TABLE_NAME2+" where "+KEY_EVENTID+" = "+eid;
	    	SQLiteDatabase db = this.getReadableDatabase();
	    	Cursor cursor = db.rawQuery(query, null);
	    	String s[] = new String[2];
	    	if(cursor.moveToFirst()){
					do{
						s[0] = String.valueOf(cursor.getInt(0));
						s[1] = cursor.getString(1);
					}while(cursor.moveToNext());
				}
				cursor.close();
				db.close();
	    	return s;
	    }
	    public boolean getPaymentStatus(int eid){
	    	String query = "select "+KEY_COORDINATORSTATUS+" from "+TABLE_NAME1+" where "+KEY_EVENTID+" = "+eid;
	    	SQLiteDatabase db = this.getReadableDatabase();
	    	Cursor cursor = db.rawQuery(query, null);
	    	boolean flag = false;
	    	if(cursor.moveToFirst()){
				if(cursor.getInt(0)==1){
					flag = true;
				}
			}
		
	    	return flag;
	    }
	    public int getPaidNumberOfStudents(int eid){
	    	String query = "select*from "+TABLE_NAME2+" where not "+KEY_PAYMENT+" = '0' and "+KEY_EVENTID+" = "+eid;
	    	SQLiteDatabase db = this.getReadableDatabase();
	    	Cursor cursor = db.rawQuery(query, null);
	    	int count = 0;
	    	if(cursor.moveToFirst()){
					do{
						++count;
					}while(cursor.moveToNext());
				}
				cursor.close();
				db.close();
	    	
	    	return count;
	    }
	    //Find Coordinators email
	    public String[] getEmail(String eid){
	    	String query = "select "+KEY_COORDINATORID+","+KEY_BRANCH+" from "+TABLE_NAME1+" where "+KEY_EVENTID+" = "+eid;

	    	SQLiteDatabase db = this.getReadableDatabase();
	    	Cursor cursor = db.rawQuery(query, null);
	    	
	    	String [] emails = new String[cursor.getCount()+2];
	    	
	    	String str =null;;
	    	int i=0;
	    	if(cursor.moveToFirst()){
	    		do{
	    			emails[i]=String.valueOf(cursor.getString(0));
	    			
	    			str =String.valueOf(cursor.getString(1));
	    		 //Toast.makeText(context,String.valueOf( cursor.getString(1)) , Toast.LENGTH_SHORT).show();

	    			i++;
	    		}while(cursor.moveToNext());
	    	}
	    	emails[i]="anudeep@skyfilabs.com";
	    	str =str.toLowerCase(Locale.ENGLISH);
	    	str=str.replaceAll("\\s+","");
	    	emails[i+1]= "register@"+str+".com" ;

			return emails;
	    	
	    }
	    
	    //check for student email
	    public boolean checkEmail(String email){
	    	
	    	String query = "select * from "+TABLE_NAME2+" where "+KEY_EMAIL+" = '"+email+"'";
	    	SQLiteDatabase db = this.getReadableDatabase();
	    	Cursor cursor = db.rawQuery(query, null);	    	
	    	if(cursor.getCount()!=0){
	    		//Toast.makeText(context, "Email already exists" ,Toast.LENGTH_LONG).show();
	    		return true;
	    		}
	    	//Toast.makeText(context, "New Email to be added" ,Toast.LENGTH_LONG).show();
			return false;
	    	
	    	
	    }
}