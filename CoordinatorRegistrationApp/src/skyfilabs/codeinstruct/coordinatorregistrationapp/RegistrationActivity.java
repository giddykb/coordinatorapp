package skyfilabs.codeinstruct.coordinatorregistrationapp;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import skyfilabs.codeinstruct.participantsdetails.AddStudentDetailsActivity;
import skyfilabs.codeinstruct.participantsdetails.Student;


import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegistrationActivity extends Activity {
	TextView venuetext,branchtext,workshoptext,paymentdetail;
	Button add,view,update;
	String venue,workshop,branch,eventid;
	int status;
	boolean flag = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		add = (Button) findViewById(R.id.register_participant);
		view = (Button) findViewById(R.id.view_participants);
		update = (Button) findViewById(R.id.update_participants);
		venuetext = (TextView)findViewById(R.id.venuetext1);
		paymentdetail = (TextView)findViewById(R.id.paymentdetail);
		workshoptext = (TextView)findViewById(R.id.workshopnametext1);
		branchtext = (TextView)findViewById(R.id.branchtext1);
		Intent intent = getIntent();
		venue = intent.getExtras().getString("venue");
		workshop = intent.getExtras().getString("workshopname");
		branch = intent.getExtras().getString("branch");
		eventid = intent.getExtras().getString("eventid");
		status = intent.getExtras().getInt("status");
		venuetext.setText(venue);
		branchtext.setText(branch);
		workshoptext.setText(workshop);
		DbHandler db = new DbHandler(getApplicationContext());
		String[] paydetail = db.getPaymentDetail(Integer.parseInt(eventid));
		boolean flag = db.getPaymentStatus(Integer.parseInt(eventid));
		int count = db.getPaidNumberOfStudents(Integer.parseInt(eventid));
		if(flag){
			if(paydetail[1]!=null){
				paymentdetail.setText("Registered Students: "+paydetail[0]+"\nAmount Received: "+paydetail[1]+"\nPaid Students: "+count);
			}else{
				paymentdetail.setText("Registered Students:"+paydetail[0]+"\nAmount Received:"+String.valueOf(0));
			}
		}else{
			paymentdetail.setText("Registered Students:"+paydetail[0]);
		}
		
	}
	public void register(View v){
		// TODO Auto-generated method stub
		Intent intent = new Intent(getApplicationContext(),AddStudentDetailsActivity.class);
		intent.putExtra("eventid",eventid);
		intent.putExtra("venue",venue);
		intent.putExtra("branch",branch);
		intent.putExtra("workshopname",workshop);
		intent.putExtra("status",status);
		startActivity(intent);
	}
	public void view(View v){
		
		// TODO Auto-generated method stub
		Intent intent = new Intent(getApplicationContext(),ViewParticipants.class);
		intent.putExtra("eventid",eventid);
		intent.putExtra("venue",venue);
		intent.putExtra("branch",branch);
		intent.putExtra("workshopname",workshop);
		startActivity(intent);
	}
	
	@SuppressLint("NewApi")
	
	public void update(View v){
		final DbHandler db = new DbHandler(getApplicationContext());

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("");
		alert.setMessage("Enter password to upload participants details");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int whichButton) {
		 String value = input.getText().toString();
		 flag = db.checkPassword(Integer.parseInt(eventid),value);
		 if(flag){
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		try{
		Toast.makeText(getApplicationContext(), "POST",Toast.LENGTH_LONG).show();
		postData1(); 
		}catch(Exception e){
		e.printStackTrace();
		}
		}else{
		Toast.makeText(getApplicationContext(),"Invalid password",Toast.LENGTH_SHORT).show();
		}
		 }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int whichButton) {
			 }
			});
		alert.show();

		}	
	
	public void postData1() throws JSONException{  
		HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://register.aerotrix.com/members/androidregs");
        JSONArray array = null;
        JSONObject object = null;
        String text = null;
        try {
        	array = new JSONArray();
        	
        	DbHandler db = new DbHandler(getApplicationContext());
    		ArrayList<Student> students_List1 = db.getStudents(eventid);
    		for(Student student : students_List1){
    			object = new JSONObject();
    			object.put("name",student.getName());
    			object.put("email",student.getEmail());
    			object.put("phone",student.getPhone());
    			object.put("id", student.getId());
    			object.put("college", student.getCollege());
    			object.put("studentid",student.getStudentId());
    			object.put("year",student.getYear());
    			object.put("eventid",student.getEventid());
    			object.put("department",student.getDepartmrnt());
    			object.put("payment",student.getPaymentid());
    			object.put("regtype","croid");
    			object.put("teamid",student.getId());
    			array.put(object);
    		}
    		
    		httppost.setHeader("json",array.toString());
            httppost.getParams().setParameter("jsonpost",array);
 
            // Execute HTTP Post Request
            
            HttpResponse response = httpclient.execute(httppost);
 
            // for JSON:
            if(response != null)
            {
                InputStream is = response.getEntity().getContent();
 
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
 
                String line = null;
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                text = sb.toString();
            }
 
            Toast.makeText(getApplicationContext(),text.toString(),Toast.LENGTH_LONG).show();
 
        }catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public void  exportToExcel(View view) throws IOException{
		
		String fileName=workshop+"-"+venue+".xls";
		File sdCard = Environment.getExternalStorageDirectory();
		String newPath = Environment.getExternalStorageState()+"/workshop/";
		File external  = new File(newPath);
		File directory = new File(sdCard.getAbsolutePath()+"/workshop");		
		directory.mkdirs();
		external.mkdirs();	
	
		File file = new File(directory,fileName);
		File exfile = new File(external,fileName);
		WorkbookSettings wbSettings = new WorkbookSettings();
		wbSettings.setLocale(new Locale("en", "EN"));
		WritableWorkbook workbook;
		
		workbook = Workbook.createWorkbook(file, wbSettings);
		
		WritableSheet sheet = workbook.createSheet("WorkshopParticipants", 0);
		try {
			sheet.addCell(new Label(0, 0, "Name"));
			sheet.addCell(new Label(1, 0, "Email"));
			sheet.addCell(new Label(2, 0, "Mobile"));
			sheet.addCell(new Label(3, 0, "Year"));
			sheet.addCell(new Label(4, 0, "Department"));
			sheet.addCell(new Label(5, 0, "Amount paid"));
			
			
			
			int i =0;
			DbHandler db = new DbHandler(getApplicationContext());
			ArrayList<Student> students_List1 = db.getStudents(eventid);
			for(Student student : students_List1){
				i=i+1;
				String name = student.getName();
				String email = student.getEmail();
				String mobile = student.getPhone();
				String year  =student.getYear();
				String department =student.getDepartmrnt();
				String amount = student.getPaymentid();
				 
				sheet.addCell(new Label(0, i, name));
				sheet.addCell(new Label(1, i, email));
				sheet.addCell(new Label(2, i, mobile));
				sheet.addCell(new Label(3, i, year));
				sheet.addCell(new Label(4, i, department));
				sheet.addCell(new Label(5, i, amount));	
				
			}
			
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			
			workbook.write();
			workbook.close();
			
			
			try{
			//sending email 
				
			Intent sendemail = new Intent(Intent.ACTION_SEND);
			String subject =  "[Participants List]["+ workshop +"][" + venue +"]";
			sendemail.setType("plain/text");
			DbHandler db = new DbHandler(getApplicationContext());
			String [] co_emails = db.getEmail(eventid);
			sendemail.putExtra(Intent.EXTRA_EMAIL, co_emails);
			sendemail.putExtra(Intent.EXTRA_SUBJECT, subject);
			//File attachment = new File("/sdcard/workshop/"+fileName);
			File attachment = new File(Environment.getExternalStorageDirectory().getPath()+"/workshop/"+fileName);			
	        Uri uri = Uri.fromFile(attachment);
	        sendemail.putExtra(Intent.EXTRA_STREAM,uri );
			sendemail.putExtra(Intent.EXTRA_TEXT,"PFA" );
			this.startActivity(Intent.createChooser(sendemail,"Sending email..."));
			
			}
			
			catch (Throwable t) {
				
				Toast.makeText(this,"Request failed try again: " + t.toString(),Toast.LENGTH_LONG).show();
			
				        }

					 

			Toast.makeText(getApplicationContext(), "Succesfully exported excel sheet", Toast.LENGTH_SHORT).show();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	
	
	@SuppressLint("NewApi")
	public void sync(View v){
		
		try{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			 StrictMode.setThreadPolicy(policy);
			 
		syncData();
		DbHandler db = new DbHandler(getApplicationContext());
		 }
		catch(Exception e){
			e.printStackTrace();
		}
	}
	

public void syncData(){
	JSONArray jArray = null;

	String result = null;

	StringBuilder sb = null;

	InputStream is = null;
	
	DbHandler db = null;
	
	try{
	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	HttpClient httpclient = new DefaultHttpClient();
	HttpPost httppost = new HttpPost("http://register.aerotrix.com/members/androidmembers/"+eventid);
	httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	HttpResponse response = httpclient.execute(httppost);
	HttpEntity entity = response.getEntity();
	 is = entity.getContent();
	}catch(Exception e1){
		Log.e("log_tag","Error in http Connection"+e1.toString());
	}
	
	try{
	      BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	       sb = new StringBuilder();
	       sb.append(reader.readLine() + "\n");

	       String line="0";
	       while ((line = reader.readLine()) != null) {
	                      sb.append(line + "\n");
	        }
	        is.close();
	        result=sb.toString();
	        }
	catch(Exception e){
	              Log.e("log_tag", "Error converting result "+e.toString());
	}
	
	try{
	      jArray = new JSONArray(result);
	      JSONObject json_data=null;
	      for(int i=0;i<jArray.length();i++){
	             json_data = jArray.getJSONObject(i);
	            
	             Student std =  new Student();
	             //json_data.getString("location");//here "Name" is the column name in database
	             //Toast.makeText(getApplicationContext(),ct_name,Toast.LENGTH_SHORT).show();
	             db = new DbHandler(getApplicationContext());
	             if(db.getStudentsCount(eventid)!=0){
	            	 String semail = json_data.getString("email");
	            	 Boolean flag =db.checkEmail(semail);
	            	  //Toast.makeText(getApplicationContext(), "Flag :"+flag, Toast.LENGTH_SHORT).show();

	            	 if(!flag){
	            		 std.setName(json_data.getString("name"));
	            		 std.setEmail(json_data.getString("email"));
	            		 std.setCollege(json_data.getString("college"));
	            		 std.setPhone(json_data.getString("phone"));
	            		 std.setPaymentid(json_data.getString("payment"));
	            		 std.setDepartmrnt("department");
	            		 std.setEventid(json_data.getString("event_id"));
	            		 std.setYear("year");
	           	         Toast.makeText(getApplicationContext(), "Loading.. ..", Toast.LENGTH_SHORT).show();

	            		 db.addStudent(std); 
	            	 }

	             }

	             else {
	            	 
	            	 std.setName(json_data.getString("name"));
	            	 std.setEmail(json_data.getString("email"));
	            	 std.setCollege(json_data.getString("college"));
	            	 std.setPhone(json_data.getString("phone"));
	            	 std.setPaymentid(json_data.getString("payment"));
	            	 std.setDepartmrnt("department");
	            	 std.setEventid(json_data.getString("event_id"));
	            	 std.setYear("year");
	       	         Toast.makeText(getApplicationContext(), "Loading ...", Toast.LENGTH_SHORT).show();
	            	 db.addStudent(std);
	             }
	             
	        }
	      Toast.makeText(getApplicationContext(), "Data Synchronized", Toast.LENGTH_SHORT).show();

	      }
	      
	      catch(JSONException e1){
	       Toast.makeText(getBaseContext(), e1.toString() ,Toast.LENGTH_LONG).show();
	      } catch (ParseException e1) {
	    	  e1.printStackTrace();
	      }
}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

}
